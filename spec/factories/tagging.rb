FactoryBot.define do
    factory :tagging, class: Tagging do
        card
        tag
    end
end
