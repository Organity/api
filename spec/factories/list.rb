FactoryBot.define do
    factory :list, class: List do
        title           "List"
        color           "Ultravioleta"
        sort            "priority"
        board          
    end
end