FactoryBot.define do
    factory :card, class: Card do
        title           "Card"
        color           "Ultravioleta"
        priority        1
        date            DateTime.strptime("19/05/2018 23:55", "%d/%m/%Y %H:%M")
    end
end