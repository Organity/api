FactoryBot.define do
    factory :tag, class: Tag do
        title           "Tag"
        color           "Ultravioleta"
        board
    end
end
