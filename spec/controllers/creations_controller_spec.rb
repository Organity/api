require 'rails_helper'

###################################
#
# Acho que não precisa mais do seed
# O seed é pra default values
# Precisa arrumar o delete cascade
#
###################################

RSpec.describe CreationsController, type: :controller do
    before :all do
        @initialLength = Creation.all.length
        @board = create(:board)
        @user = create(:user)
        @creation = create(:creation, board: @board, user: @user)
        @jsonCreation = {
            "user_id" => @creation.user.id,
            "board_id" => @creation.board.id
        }
    end

    after :all do
        @creation.destroy 
        @board.destroy
        @user.destroy
    end

    describe "GET #index" do
        it "returns all boards" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns board with the given index" do
            get :show, params: {id: @creation.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@creation.id)
        end

        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("Creation not found!")
        end
    end

    describe "POST #create" do
        it "creates a new board in data base" do
            expect{
                post :create, params: { creation: @jsonCreation}
            }.to change(Creation, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a board" do
            put :update, params: {id: @creation.id, creation: @jsonCreation}
            body = JSON.parse(response.body)
            expect(body["user_id"]).to eq(@creation.user.id)
        end
        
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, creation: @jsonCreation}
            expect(resp.body).to eq("Creation not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete board with given index" do
            expect{
                delete :destroy, params: {id:@creation.id}
            }.to change(Creation, :count).by(-1)
        end
    end

    
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("Creation not found!")
        end
end