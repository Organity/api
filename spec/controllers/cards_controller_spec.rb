require 'rails_helper'

RSpec.describe CardsController, type: :controller do
    before :all do
        @initialLength = Card.all.length
        @card = create(:card, 
                        title: "Hello",
                        color: "World",
                        priority: 2,
                        date: DateTime.strptime("19/01/2018 23:55", "%d/%m/%Y %H:%M")
        )
        @new = build_stubbed(:card)
        @jsonNew = {
                "title" => @new.title,
                "color" => @new.color,
                "priority" => @new.priority,
                "date" => @new.date
        }
    end

    after :all do 
        @card.destroy
    end

    describe "GET #index" do
        it "returns all cards" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns card with the given index" do
            get :show, params: {id: @card.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@card.id)
        end
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("Card not found!")
        end
    end

    describe "POST #create" do
        it "creates a new card in data base" do
            expect{
                post :create, params: { card: @jsonNew}
            }.to change(Card, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a card" do
            put :update, params: {id: @card.id, card: @jsonNew}
            body = JSON.parse(response.body)
            expect(body["color"]).to eq("Ultravioleta")
        end

        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, list: @jsonCardUpdate}
            expect(resp.body).to eq("Card not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete card with given index" do
            expect{
                delete :destroy, params: {id:@card.id}
            }.to change(Card, :count).by(-1)
        end
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("Card not found!")
        end
    end
end