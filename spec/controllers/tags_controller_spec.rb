require 'rails_helper'

RSpec.describe TagsController, type: :controller do
    before :all do
        @initialLength = Tag.all.length
        @board = create(:board)
        @tag = create(:tag, board: @board)
        
        @new = build_stubbed(:tag, board: @board)
        @jsonTag = {
            "title" => @new.title,
            "color" => @new.color,
            "board_id" => @new.board.id
        }
    end

    after :all do
        @tag.destroy
        @board.destroy
    end
    
    describe "GET #index" do
        it "returns all tags" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns tag with the given index" do
            get :show, params: {id:@tag.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@tag.id)
        end
        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("Tag not found!")
        end
    end

    describe "POST #create" do
        it "creates a new tag in data base" do
            jsonTag = {
                "title" => @new.title,
                "color" => @new.color,
                "board_id" => @new.board.id
            }
            expect{
                post :create, params: { tag: jsonTag}
            }.to change(Tag, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a tag" do
            jsonTag = {
                "title" => @new.title,
                "color" => @new.color,
            }
            put :update, params: {id: @tag.id, tag: jsonTag}
            body = JSON.parse(response.body)
            expect(body["color"]).to eq("Ultravioleta")
        end
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, list: @jsonTagUpdate}
            expect(resp.body).to eq("Tag not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete tag with given index" do
            expect{
                delete :destroy, params: {id:@tag.id}
            }.to change(Tag, :count).by(-1)
        end
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("Tag not found!")
        end
    end
end
