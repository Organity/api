require 'rails_helper'

###################################
#
# Acho que não precisa mais do seed
# O seed é pra default values
# Precisa arrumar o delete cascade
#
###################################

RSpec.describe ListingsController, type: :controller do
    before :all do
        @initialLength = Listing.all.length
        @board = create(:board)
        @card = create(:card)
        @list = create(:list, board: @board)
        @listing = create(:listing, card: @card, list: @list)
        
        @jsonListing = {
            "card_id" => @listing.card.id,
            "list_id" => @listing.list.id
        }
    end

    after :all do 
        @listing.destroy
        @card.destroy
        @list.destroy
        @board.destroy
    end

    describe "GET #index" do
        it "returns all listings" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns listing with the given index" do
            get :show, params: {id: @listing.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@listing.id)
        end

        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("listing not found!")
        end
    end

    describe "POST #create" do
        it "creates a new listing in data base" do
            expect{
                post :create, params: { listing: @jsonListing}
            }.to change(Listing, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a listing" do
            put :update, params: {id: @listing.id, listing: @jsonListing}
            body = JSON.parse(response.body)
            expect(body["card_id"]).to eq(@listing.card.id)
        end
        
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, listing: @jsonListing}
            expect(resp.body).to eq("listing not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete listing with given index" do
            expect{
                delete :destroy, params: {id:@listing.id}
            }.to change(Listing, :count).by(-1)
        end
    end

    
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("listing not found!")
        end
end
