require 'rails_helper'

RSpec.describe ListsController, type: :controller do
    before :all do
        @initialLength = List.all.length
        @board = create(:board)
        @list = create(:list, board: @board)

        @new = build_stubbed(:list, board: @board)
        @jsonListCreate = {
            "title" => @new.title,
            "color" => @new.color,
            "sort" => @new.sort,
            "board_id" => @new.board.id
        }

        @jsonListUpdate = {
            "title" => @new.title,
            "color" => @new.color,
            "sort" => @new.sort
        }
    end

    after :all do
        @list.destroy
        @board.destroy
    end

    describe "GET #index" do
        it "returns all lists" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns list with the given index" do
            get :show, params: {id:@list.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@list.id)
        end
        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("List not found!")
        end
    end

    describe "POST #create" do
        it "creates a new list in data base" do
            jsonListCreate = {
                "title" => @new.title,
                "color" => @new.color,
                "sort" => @new.sort,
                "board_id" => @new.board.id
            }
            expect{
                resp = post :create, params: { list: jsonListCreate}
            }.to change(List, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a list" do
            put :update, params: {id: @list.id, list: @jsonListUpdate}
            body = JSON.parse(response.body)
            expect(body["color"]).to eq("Ultravioleta")
        end
        
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, list: @jsonListUpdate}
            expect(resp.body).to eq("List not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete list with given index" do
            expect{
                delete :destroy, params: {id:@list.id}
            }.to change(List, :count).by(-1)
        end


        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("List not found!")
        end
    end
end
