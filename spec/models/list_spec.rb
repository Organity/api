require 'rails_helper'

RSpec.describe List, type: :model do
    
    before :all do
        @list = create(:list)
        @invalid = build(:list, id: @list.id)
    end

    after :all do
        @list.destroy
    end

    it 'must be a valid with valid attributes' do
        expect(@list).to be_valid
    end

    it 'must be invalid with same id' do
        expect{ @invalid.save! }.to raise_error( ActiveRecord::RecordNotUnique )
    end
end
