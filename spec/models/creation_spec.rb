require 'rails_helper'

RSpec.describe Creation, type: :model do
    
    before :all do
        @creation = build_stubbed(:creation)
    end

    it 'belongs to a tag' do
        expect(@creation.user.name).to eq("João")
    end

    it 'belongs to a card' do
        expect(@creation.board.title).to eq("Board")
    end
end
