require 'rails_helper'

RSpec.describe Tag, type: :model do
    
    before :all do
        @tag = create(:tag)
        @invalid = build(:tag, id: @tag.id)
    end

    after :all do
        @tag.destroy
    end

    it 'must be a valid with valid attributes' do
        expect(@tag).to be_valid
    end

    it 'must be invalid with same id' do
        expect{ @invalid.save! }.to raise_error( ActiveRecord::RecordNotUnique )
    end
end


