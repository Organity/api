require 'rails_helper'

RSpec.describe Tagging, type: :model do
    
    before :all do
        @tagging = build_stubbed(:tagging)
    end

    it 'belongs to a tag' do
        expect(@tagging.tag.title).to eq("Tag")
    end

    it 'belongs to a card' do
        expect(@tagging.card.title).to eq("Card")
    end
end



