require 'rails_helper'

RSpec.describe Listing, type: :model do
    
    before :all do
        @listing = build_stubbed(:listing)
    end

    it 'belongs to a list' do
        expect(@listing.list.title).to eq("List")
    end

    it 'belongs to a card' do
        expect(@listing.card.title).to eq("Card")
    end
end




