require 'rails_helper'

RSpec.describe User, type: :model do
    before :all do
        @user = create(:user)
        @invalid_id = build(:user, id: @user.id, email: "m@ria.com")
        @invalid_email = build(:user)
    end
   
    after :all do
        @user.destroy
    end

    it 'must be a valid with valid attributes' do
        expect(@user).to be_valid
    end

    it 'must be invalid with same id' do
        expect{ @invalid_id.save! }.to raise_error( ActiveRecord::RecordNotUnique )
    end

    it 'must be invalid with same email' do
        expect{ @invalid_email.save! }.to raise_error( ActiveRecord::RecordInvalid )
    end
end 
