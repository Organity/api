require 'rails_helper'

RSpec.describe Card, type: :model do
    
    before :all do
        @card = create(:card)
        @invalid = build(:card, id: @card.id)
    end

    after :all do
        @card.destroy
    end

    it 'must be a valid with valid attributes' do
        expect(@card).to be_valid
    end

    it 'must be invalid with same id' do
        expect{ @invalid.save! }.to raise_error( ActiveRecord::RecordNotUnique )
    end
end

