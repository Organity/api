################################################################################
#                                 BASE IMAGE                                   #
################################################################################

# Using official Ruby's alpine image as base
FROM ruby:2.5-alpine AS base

# Set the path where the files will be stored in the container
ENV API_PATH=/usr/src/api

# Environment variables required for bundle
ENV BUNDLE_PATH="vendor/bundle" \
    PATH="$API_PATH/bin:$PATH"

# Environment variables for the container network
ENV PORT=3000 \
    HOST=0.0.0.0

# Expose default port to connect with the service
EXPOSE $PORT

# Create the directory defined by $API_PATH (if doesn't exist) and cd into it
WORKDIR $API_PATH

################################################################################
#                          DEVELOPMENT IMAGE                                   #
################################################################################

# Expanding base image as development image
FROM base AS development

# Set the environment for rails
ENV RAILS_ENV=development

# Install dependencies that are native packages
RUN apk add --update \
    build-base \
    postgresql-dev \
    tzdata \
    && rm -rf /var/cache/apk/*

# Copy Gemfile and Gemfile.lock to install gem dependencies
COPY Gemfile Gemfile.lock ./

# Install gem dependencies
RUN bundle install

# Copy the application code into the container
COPY . .

# Define the the default command to execute when container is run
CMD ["rails", "server"]

################################################################################
#                           PRODUCTION IMAGE                                   #
################################################################################

# Expanding base image as development image
FROM base AS production

# Set the environment for rails
ENV RAILS_ENV=production

# Install dependencies that are native packages
RUN apk add --update \
    libressl-dev \
    postgresql-libs \
    tzdata \
    && rm -rf /var/cache/apk/*

# Copy code from development container
COPY --from=development $API_PATH/ ./ ./

# Remove development dependencies
RUN bundle --clean --frozen --without development test

# Define the the default command to execute when container is run
CMD ["rails", "server"]
