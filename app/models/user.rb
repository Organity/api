class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :creations, :dependent => :destroy
  has_many :boards, :through => :creations

  include DeviseTokenAuth::Concerns::User
end
