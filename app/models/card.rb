class Card < ApplicationRecord
  has_many :listings, :dependent => :destroy
  has_many :taggings, :dependent => :destroy
  has_many :lists, :through => :listings
  has_many :tags, :through => :taggings
end
