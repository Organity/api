class BoardsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_board, only: [:show, :update, :destroy]

  # GET /boards
  def index
    p @current_user

    @boards = Board.all

    render json: @boards
  end

  # GET /boards/1
  def show
    render json: @board

  end

  # POST /boards
  def create
    @board = Board.new(board_params)

    if @board.save
      render json: @board, status: :created, location: @board
    else
      render json: @board.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /boards/1
  def update
    if @board != "Board not found!"
      if @board.update(board_params)
        render json: @board
      else
        render json: @board.errors, status: :unprocessable_entity
      end
    else
      render json: @board
    end
  end

  # DELETE /boards/1
  def destroy
    if @board != "Board not found!"
      @board.destroy
    else
      render json: @board
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board
      if params[:user_id]
        @board = User.find(params[:user_id]).boards
        return
      end

      @board = Board.find_by_id(params[:id])
      if !@board
        @board = "Board not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def board_params
      params.require(:board).permit(:title, :color, :period, :visualization)
    end
end
