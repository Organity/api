class TagsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_tag, only: [:show, :update, :destroy]
  before_action :set_card, only: [:create]

  # GET /tags
  def index
    @tags = Tag.all

    render json: @tags
  end

  # GET /tags/1
  def show
    render json: @tag
  end

  # POST /tags
  def create
    @tag = Tag.find(params[:id])

    if @tag
      @card.tags << @tag
      @card.save
      p @tag
    else
      @tag = Tag.new(tag_params)
    end

    if @tag.save
      render json: @tag, status: :created, location: @tag
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tags/1
  def update
    if @tag != "Tag not found!"
      if @tag.update(tag_params)
        render json: @tag
      else
        render json: @tag.errors, status: :unprocessable_entity
      end
    else
      render json: @tag
    end
  end

  # DELETE /tags/1
  def destroy
    if @tag != "Tag not found!"
      @tag.destroy
    else
      render json: @tag
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      if params[:card_id]
        @tag = Card.find(params[:card_id]).tags
        return
      end

      if params[:board_id]
        @tag = Board.find(params[:board_id]).tags
        return
      end

      @tag = Tag.find_by_id(params[:id])
      if !@tag
        @tag = "Tag not found!"
      end
    end

    def set_card
      if params[:card_id]
        @card = Card.find(params[:card_id])
      end
    end

    # Only allow a trusted parameter "white list" through.
    def tag_params
      params.require(:tag).permit(:title, :color, :board_id)
    end
end
