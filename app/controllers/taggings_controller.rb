class TaggingsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_tagging, only: [:show, :update, :destroy]

  # GET /taggings
  def index
    @taggings = Tagging.all

    render json: @taggings
  end

  # GET /taggings/1
  def show
    render json: @tagging
  end

  # POST /taggings
  def create
    @tagging = Tagging.new(tagging_params)

    if @tagging.save
      render json: @tagging, status: :created, location: @tagging
    else
      render json: @tagging.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /taggings/1
  def update
    if @tagging != "tagging not found!"
      if @tagging.update(tagging_params)
        render json: @tagging
      else
        render json: @tagging.errors, status: :unprocessable_entity
      end
    else
      render json: @tagging
    end
  end

  # DELETE /taggings/1
  def destroy
    if @tagging != "tagging not found!"
      @tagging.destroy
    else
      render json: @tagging
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tagging
      @tagging = Tagging.find_by_id(params[:id])
      if !@tagging
        @tagging = "tagging not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def tagging_params
      params.permit(:card_id, :tag_id)
    end
end
