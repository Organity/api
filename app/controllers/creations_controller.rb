class CreationsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_creation, only: [:show, :update, :destroy]

  # GET /creations
  def index
    @creations = Creation.all

    render json: @creations
  end

  # GET /creations/1
  def show
    render json: @creation
  end

  # POST /creations
  def create
    @creation = Creation.new(creation_params)

    if @creation.save
      render json: @creation, status: :created, location: @creation
    else
      render json: @creation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /creations/1
  def update
    if @creation != "Creation not found!"
      if @creation.update(creation_params)
        render json: @creation
      else
        render json: @creation.errors, status: :unprocessable_entity
      end
    else
      render json: @creation
    end
  end

  # DELETE /creations/1
  def destroy
    if @creation != "Creation not found!"
      @creation.destroy
    else
      render json: @creation
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_creation
      @creation = Creation.find_by_id(params[:id])
      if !@creation
        @creation = "Creation not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def creation_params
      params.permit(:user_id, :board_id)
    end
end
