class CardsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_card, only: [:show, :update, :destroy]

  # GET /cards
  def index
    @cards = Card.all

    render json: @cards
  end

  # GET /cards/1
  def show
    render json: @card
  end

  # POST /cards
  def create
    @card = Card.new(card_params)

    if @card.save
      render json: @card, status: :created, location: @card
    else
      render json: @card.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /cards/1
  def update
    if @card != "Card not found!"
      if @card.update(card_params)
        render json: @card
      else
        render json: @card.errors, status: :unprocessable_entity
      end
    else
      render json: @card
    end
  end

  # DELETE /cards/1
  def destroy
    if @card != "Card not found!"
      @card.destroy
    else
      render json: @card
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card

      if params[:tag_id]
        @card = Tag.find(params[:tag_id]).cards
        return
      end

      if params[:list_id]
        @card = List.find(params[:list_id]).cards
        return
      end

      @card = Card.find_by_id(params[:id])
      if !@card
        @card = "Card not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def card_params
      params.require(:card).permit(:title, :color, :priority, :date)
    end
end
