class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string :title
      t.string :color
      t.references :board, foreign_key: true

      t.timestamps
    end
  end
end
