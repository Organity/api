class CreateBoards < ActiveRecord::Migration[5.1]
  def change
    create_table :boards do |t|
      t.string :title
      t.string :color
      t.string :period
      t.string :visualization

      t.timestamps
    end
  end
end
